import base64
import json
import os
from google.cloud import pubsub_v1


publisher = pubsub_v1.PublisherClient()
PROJECT_ID = os.getenv('MY_GOOGLE_PROJECT')                             # GOOGLE_CLOUD_PROJECT


def hologram_to_pubsub(request):
    data = request.data
    
    if data is None:
        print('Hologram payload for POST DATA not found')
        return ('Hologram payload for POST DATA not found', 400)

    print(f'request data: {data}')                                      # this is the readings_json string from hologramCloudTest.py sent out by the Hologram Nova
    
    data_json = json.loads(data)                                        # turn the string into a dictionary
    print(f'request data json = {data_json}')

    sensor_name = data_json['sensorName']
    temperature = data_json['temperature']
    humidity = data_json['humidity']
    
    print(f'sensor_name = {sensor_name}')
    print(f'temperature = {temperature}')
    print(f'humidity = {humidity}')


    # myValue = request.headers.get('myKey')                            # on Hologram Cloud's Routes, if you use the section of "HEADERS, Header key/value pairs to be included in the POST request." you can get the data like this
    # print(f'request headers.get myKey: {myValue}')

    ###############################
    # move the data to Pubsub!

    topic_path = 'projects/MY_GOOGLE_PROJECT/topics/MY_TOPIC'           # Pubsub topic path

    message_json = json.dumps({
        'data': {'message': 'sensor readings!'},
        'readings': {
            'sensorName': sensor_name,
            'temperature': temperature,
            'humidity': humidity
        }
    })
    message_bytes = message_json.encode('utf-8')

    try:
        publish_future = publisher.publish(topic_path, data=message_bytes)
        publish_future.result()                                         # verify that the publish succeeded
    except Exception as e:
        print(e)
        return (e, 500)

    return ('Message received and published to Pubsub', 200)