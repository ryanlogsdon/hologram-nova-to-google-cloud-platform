import json
from Hologram.HologramCloud import HologramCloud

if __name__ == '__main__':
    try:
        hologram = HologramCloud(dict(), network='cellular')
        print(f'Cloud type: {str(hologram)}')

        result = hologram.network.connect()
        if result == False:
            print('Filed to connect to cellular network')

        readings = {
            'sensorName': 'garden-sensor-001',
            'temperature': 76.4, 
            'humidity':100
        }
        readings_json = json.dumps(readings)

        response_code = hologram.sendMessage(readings_json, topics=['MY_TOPIC'], timeout=3)
        # response_code = hologram.sendMessage('{"temperature": 75, "humidity":100}', topics=['_DEVICE_1051145_'], timeout=3)
        result_string = hologram.getResultString(response_code)
        print(result_string)


        hologram.network.disconnect()
    except Exception as e:
        print(f'Error: {e}')
